/* tslint:disable:no-console */
import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import "mocha";

chai.use(chaiAsPromised);
const expect = chai.expect;
chai.should();

import { dummyPromise, staggerPromises, StaggerPromisesResult, wait } from "../src";

describe("Async", () => {
  describe("dummyPromise()", () => {
    it("can resolve a dummy promise", async () => {
      return expect(dummyPromise(55))
        .to.eventually.equal(55);
    });
  });

  describe("staggerPromises()", () => {
    it("can await promises on a delay", async () => {
      const startTime = new Date().getTime();
      await staggerPromises(generateTestPromiseArray(5), 500)
        .then((res: StaggerPromisesResult) => {
          expect(res.promisesFulfilled).to.equal(5);
          expect(res.results).to.be.an("array").with.length(5);
          const endTime = new Date().getTime();
          const timeElapsed = endTime - startTime;
          expect(timeElapsed).to.be.above(2000);
      });
    }).timeout(3000);

  });

  describe("wait()", () => {
    it("can wait", async () => {
      const startTime = new Date().getTime();
      await wait(2000);

      const endTime = new Date().getTime();
      const timeElapsed = endTime - startTime;
      expect(timeElapsed).to.be.above(1975);
    }).timeout(2500);

  });

});

function generateTestPromiseArray(n: number): Array<Promise<any>> {
  const tmp: any[] = [];
  for (let i = 0; i < n; i++) {
    tmp.push(dummyPromise(i));
  }
  return tmp;
}
