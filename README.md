#Summary
    Collection of async and related utility functions

    Work in progress, use at own risk!

    import { dummyPromise, staggerPromises, wait } from "bsh-async-utils";


#DummyPromise
    An empty promise that resolves what it's given

    try {
      const result = await dummyPromise(42);
      console.log(result);
    } catch (e) {
      throw e;
    }


#StaggerPromises
    Takes an array of promises and awaits them in series with a delay

    console.log("start:", new Date().getTime());
    const promises = generateTestPromiseArray(6);
    staggerPromises(promises, 500)
      .then((results: any) => {
        console.log("results:", results);
        console.log("end:", new Date().getTime());
      });


#Wait
    Hard sleep function

    console.log("start:", new Date().getTime());
    await wait(1000);
    console.log("end:", new Date().getTime());
