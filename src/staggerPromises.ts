/* tslint:disable:no-console */
export function staggerPromises(promises: Array<Promise<any>>, delay: number, i: number = 0, data: any[] = []) {
  return new Promise((resolve, reject) => {
    if (delay < 1) {
      delay = 1;
    }

    if (promises.length === 0) {
      resolve({ promisesFulfilled: i, results: data } as StaggerPromisesResult);
    } else {
      const p = promises.shift();
      if (p) {
        p.then((x: any) => {
          data.push({ resolvedAt: new Date().getTime(), resolution: x } as Result);
          setTimeout(() => {
            i++;
            resolve(staggerPromises(promises, delay, i, data));
          }, delay);
        });
      }
    }
  });
}

export interface StaggerPromisesResult {
  promisesFulfilled: number;
  results: Result[];
}

export interface Result {
  resolution: any;
  resolvedAt: number;
}
