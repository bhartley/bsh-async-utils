export function wait(delay: number) {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, delay);
  });
}
